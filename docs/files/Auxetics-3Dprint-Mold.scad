  /*
  // FILE    : Auxetics Metamaterials with holes - Middle plate
  // VERSION : 1.0
  // LAST UPDATE : July 21 2020
  // LICENSE : M.I.T. (https://opensource.org/licenses/mit-license.php)
  // AUTHOR  : Denis Terwagne
  // TARGET  : OpenSCAD
  */

kerf=0;

//height
height=8;
//height/base
height_base=1;
//dowel pins diameter
diam=8;
//space in between holes
space=diam/5;
//number of holes horizontal and vertical
nb_horiz=5;
nb_vert=5;

base=3;
out_case=6;

translate([0,0,-height_base/2]) cube(size=[(nb_horiz+1)*diam+(nb_horiz+1)*space+out_case,nb_vert*diam+(nb_vert-1)*space+2*base+out_case,height_base],center=true);
//Middle plate
union(){
difference(){
    translate([0,0,height/2]) cube(size=[(nb_horiz+1)*diam+(nb_horiz+1)*space+out_case,nb_vert*diam+(nb_vert-1)*space+2*base+out_case,height],center=true);
    translate([0,0,height/2]) cube(size=[(nb_horiz+1)*diam+(nb_horiz+1)*space,nb_vert*diam+(nb_vert-1)*space+2*base,height],center=true);
    }

   for (i=[0:1:(nb_vert-1)]){
    translate([-(diam+space)*(nb_horiz+1)/2,(diam+space)*(i-(nb_vert-1)/2),0])cylinder($fn = 100,height,d=(diam+kerf));
    translate([(diam+space)*(nb_horiz+1)/2,(diam+space)*(i-(nb_vert-1)/2),0])cylinder($fn = 100,height,d=(diam+kerf));
   }
}





//Upper and lower plates

//cylinder($fn = 100,height,d=(diam-kerf));

for (i=[0:1:(nb_vert-1)]){
    translate([0,(diam+space)*(i-(nb_vert-1)/2),0])
    for (i=[0:1:(nb_horiz-1)]){
         translate([(diam+space)*(i-(nb_horiz-1)/2),0,0])cylinder($fn = 100,height,d=(diam-kerf));
         translate([-(diam+space)*(i-(nb_horiz-1)/2),0,0])cylinder($fn = 100,height,d=(diam-kerf));
    }
}

 
                
                