# Material list

In this online workshop, there are 4 activities that are proposed. Some will require more materials and equipment than others. These activities has been specially designed if you have access to the digital tools of a Fab Lab. One activity will only need your hands and fingers (+ scissors).

### Workshop information:

Conference: FABXLive 27-31 July 2021  
Workshop date: July 28 2020  
Start time: 19:00 UTC+2  
Duration: 2h  
Speakers :

* Denis Terwagne (FabLab ULB, Université Libre de Bruxelles ULB, Belgium),
* Julien Chopin (Universidade Federal da Bahia, Salvadore, Brazi

### Get ready

1. Choose one or multiple activities (from the list below) you want to make during the time of the workshop.
2. Before the workshop, gather all the materials and equipment you need to do the activity/ies you chose.
3. Prepare in advance steps that take time such as 3d printing,...
4. Review the tutorials and make sure you are ready for the day of the workshop.

### Activities

####   Stretchable kirigami

[Tutorial](http://fab.academany.org/2018/labs/fablabulb/FAB14_workshop_kirigami.html)

<div class="row">
 <div class="column-small">
   <img src="../images/kirigami_web.jpg" alt="Snow" style="width:100%">
 </div>
 <div class="column-large">

  Materials
<ul>    
      <li> PET sheets (few hundred micrometers in thickness)</li>
      or
      <li> Polypropylene plastic sleeves (easy to find option)</li>
      or
      <li> paper sheets </li>
</ul>

  Machine

  <ul>
        <li> Laser cutter</li>
        or
        <li> Vinyl cutter</li>
 </ul>

 </div>

</div>

####   Bistable and auxetics mechanical metamaterials

[Tutorial](http://fab.academany.org/2018/labs/fablabulb/FAB14_workshop_bistable.html)

<div class="row">
 <div class="column-small">
   <img src="../images/bistable_auxetics.jpg" alt="Snow" style="width:100%">
 </div>
 <div class="column-large">

 Materials
<ul>
     <li>  <a href="https://www.trodat.net/fr-FR/produits/Consommables/Caoutchouc-laser/Pages/produits.aspx">laserable rubber sheets</a>  (2 to 3 mm in thickness, no odor if possible) </li>

</ul>

 Machine

 <ul>
       <li> Laser cutter</li>
       <li> cutter or knife </li>
</ul>

 </div>

</div>

#### Auxetics mechanical metamaterials (option 1)

[Tutorial](http://fab.academany.org/2018/labs/fablabulb/FAB14_workshop_auxetics.html)

<div class="row">
 <div class="column-small">
   <img src="../images/auxetics_close_web.jpg" alt="Snow" style="width:100%">
 </div>
 <div class="column-large">

 Preparation
 <ul>
       <li> You need to laser cut the acrylic and buy/find the dowel pins in advance. </li>
 </ul>

 Materials
<ul>
     <li> Acrylic sheets (thicknesses: 5 mm and 8 mm) </li>
     <li> >50 dowel pins (6, 8 or 10 mm of diameter) </li>
     <li> Scale </li>
     <li> Silicone based polymer (fast curing < 30 min) </li>
     <li> Mixing cups </li>
     <li> Mixing sticks </li>
</ul>

 Machine

 <ul>
       <li> Laser cutter</li>
</ul>

 </div>

</div>

#### Auxetics mechanical metamaterials (option 2)

[Tutorial to adapt](http://fab.academany.org/2018/labs/fablabulb/FAB14_workshop_auxetics.html)

<div class="row">
 <div class="column-small">
   <img src="../images/auxetics_close_web.jpg" alt="Snow" style="width:100%">
 </div>
 <div class="column-large">

 Preparation
 <ul>
       <li> print this <a href="../files/mold.STL">3D mold</a> in advance </li>
</ul>
 Materials
<ul>
     <li> PLA, PETG, ... 3D printing materials </li>
     <li> Scale </li>
     <li> Silicone based polymer (fast curing < 30 min) </li>
     <li> Mixing cups </li>
     <li> Mixing sticks </li>
</ul>

 Machine

 <ul>
       <li> 3D printer</li>
</ul>

 </div>

</div>

#### Twist and fold origami

[Tutorial to appear]()

<div class="row">
 <div class="column-small">
   <img src="../images/twist-fold-origami.jpg" alt="Snow" style="width:100%">
 </div>
 <div class="column-large">

 Materials
<ul>
     <li> Paper sheets </li>
</ul>

 Machine

 <ul>
       <li> Scissors</li>
       <li> ruler</li>
       <li> your hands and fingers</li>
</ul>

 </div>

</div>
