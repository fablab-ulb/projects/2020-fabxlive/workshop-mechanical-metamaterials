# Stretchable kirigami

Materials

* PET sheets (few hundred micrometers in thickness)
or
* Polypropylene plastic sleeves (easy to find option)
or
* paper sheets

Machine

* Laser cutter
or
* cutter or knife

## Introduction

Here, we are going to make mechanical metamaterials from a simple kirigami. The particularity of this kirigami is that its longitudinal rigidity can be tuned depending on the design parameters.

Isobe et al. [Isobe, Scientific Reports, 2016](https://www.nature.com/articles/srep24758) have found a relation between the longitudinal rigidity of the structure and the design parameters.

![](../images/Isobe.jpg)

## Kirigami pattern generator:

Use the following script to create a vector image that can be cut on a flexible sheet using a laser cutter or a Vinyl cutter.

<form id="flexForm">
      <table>
        <tbody>
          <tr>
            <td>Width</td>
            <td><input id="size_x" type="number" min="1" value="100" step="0.1"></td>
          </tr>
          <tr>
        <td>Height</td>
        <td><input id="size_y" type="number" min="1" value="200" step="0.1"></td>
      </tr>
      <tr>
        <td>Cut to border distance</td>
        <td><input id="toBorder" type="number" min="1" value="5" step="0.1"></td>
      </tr>
      <tr>
        <td>Top Blank (exact)</td>
        <td><input id="topBlank" type="number" min="1" value="25" step="0.1"></td>
      </tr>
      <tr>
        <td>Bottom Blank (approximate)</td>
        <td><input id="bottomBlank" type="number" min="1" value="25" step="0.1"></td>
      </tr>
      <tr>
        <td>Space between lines</td>
        <td><input id="interLine" type="number" min="1" value="5" step="0.1"></td>
      </tr>
      <tr>
        <td>Number of lines</td>
        <td>
<!--    		<select id="cutPattern">
    			<option value="0" >0</option>
    			<option value="1" selected>1</option>
    			<option value="2">2</option>
    			<option value="3">3</option>
    		</select>-->
        <input id="cutPatternArg" type="number" min="1" value="4">
      </tr>
      <tr>
        <td>Add border</td>
        <td><input id="isBorder" type="checkbox" checked></td>
      </tr>

    </tbody>
    <tfoot>
      <tr>
    	<td colspan="2" style="padding:12px;">
            <p>Note : you must update SVG before copy/download.</p>
            <input type="button" value="See / Update SVG" onclick="updateSVG('flexSVG')">
            <input type="button" value="Copy to clipboard" onclick="copyToClipboard($('#flexSVG').html())">
            <input type="button" value="Download as File" onclick="downloadAsFile('Vigon_FlexWood_Generator.svg', $('#flexSVG').html())">
            <input type="button" value="Clear" onclick="$('#flexSVG').html('')">
          </td>
        </tr>
      </tfoot>
      </table>
    </form>

<script>
function updateSVG(svgID){
var $target   = $("#"+svgID);
var width     = parseFloat($("#size_x").val());
var height    = parseFloat($("#size_y").val());
var topBlank  = parseFloat($("#topBlank").val());
var bottomBlank  = parseFloat($("#bottomBlank").val());
var toBorder  = parseFloat($("#toBorder").val());
var interLine = parseFloat($("#interLine").val());
var cutPattern= parseInt($("#cutPattern").val());
var cutPatternArg= parseInt($("#cutPatternArg").val());

var nLines=Math.floor((height-topBlank-bottomBlank)/interLine);
var svgPaths = '<svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="'+width+'mm" height="'+height+'mm" viewBox="0 0 '+width+' '+height+'">'+"\n";
svgPaths += '<g stroke="black">'+"\n";

switch (cutPattern) {
  default:
    var length = (width - (2*cutPatternArg-1)*toBorder)/(cutPatternArg-0.5);
    for(var i=0; i < nLines; i++){
      if(i%2){svgPaths += "\t"+'<g transform="translate('+width+',0)scale(-1,1)">'+"\n";}
      for(var j=0; j < cutPatternArg; j++){
        svgPaths += "\t"+'<path d="';
        svgPaths += 'M ' + (toBorder*(2*j+1) + length*j) + ',' + (topBlank+(i*interLine)) + ' ';
        if(j==cutPatternArg-1){
          svgPaths += 'H ' + width;
        }
        else {
          svgPaths += 'H ' + (toBorder*(2*j+1) + length*(j+1));
        }
        svgPaths += '"/>'+"\n";
      }
      if(i%2){svgPaths += "\t"+'</g>'+"\n";}
    }
}

if($("#isBorder").prop('checked')){
  svgPaths += '<rect width="'+width+'" height="'+height+'" fill="none" />'+"\n";
}
svgPaths += '</g>'+"\n";
svgPaths += '</svg>'+"\n";
$target.html(svgPaths);
}
</script>
<div class="text-center">
<!-- Figure that will be filled with the flex Wood script -->
<figure id="flexSVG"></figure>
</div>

## Testing the structures

Lasercut the structure in a paper or a PET thin sheet and feel how the rigidity change for different design parameters.

![](../images/kirigami_web.jpg)
