# Mechanical Metamaterials : Folds and Cuts

We will introduce fundamental principles of mechanical metamaterials that have unusual mechanical properties thanks to their structure. These will be illustrated using visual and hands-on examples by folding and cutting sheets.

## Workshop information:

Conference: FABXLive 27-31 July 2021  
Workshop date: July 28 2020  
Start time: 19:00 UTC+2  
Duration: 2h  
Speakers :

* Denis Terwagne (FabLab ULB, Université Libre de Bruxelles ULB, Belgium),
* Julien Chopin (Universidade Federal da Bahia, Salvadore, Brazil)

## Summary

Mechanical metamaterials are materials that have unusual mechanical properties thanks to their structure rather than their microscopic composition. Digital fabrication has recently given the tools to physicists, mathematicians, engineers and designers to explore these kinds of new materials. These smart materials open the path to materials in which we can pre-program the way they deform or they behave. In this workshop, we would like to show some fundamental and recent discoveries in the field of mechanical metamaterials. Although the example we chose to present have been flavored by the scientific community, they are often inspired from artistic or nature's work. We will give to this workshop a very hands-on and visual flavor to convey the key messages. These will be illustrated by building origami and kirigami foldings and by manipulating experimental and physical models. In the future, these might inspire new practice in mechanical design.

![](images/bistable_auxetics.jpg)

## Activities

1. Explore the different types of mechanical materials, and choose one.
2. Use a design software (inkscape, illustrator, openscad, ...) to design digital mechanical building blocks (folds, hinges, elastic beams,... )
3. Fabricate a functional building block. Check the design rules.
4. Design and fabricate a structure that is made of a combination of building blocks (stretchable, bistable or auxetics). The structure has to do something unexpected when deform/compressed.


## Mechanical metamaterials
The goal of this workshop is to make mechanical metamaterials that are made of mechanical building blocks. The mechanical functions of these structures are not only given by the instrinsinc properties of the material but by their geometrical structure.

Exploring these classes of structures is really eased by digital fabrication. The design of these structure can be made parametrically which allow us to fabricate a large variety of test samples and explore physically the complete space parameters. It's unleashing the power of numerical simulation in the physical world (this is a real revolution in how problems are tackled in Science).

One kind of mechanical metamaterials can be made with kirigami. Kirigami is simply a sheet folding in which there are cuts. When the 2D structure is stretched, it deforms in a particular way: it can expand, retract, rotate, move out of plane,...

There are different types of mechanical metamaterials that you will discover by browsing this website and the following youtube channel :

<iframe width="560" height="315" src="https://www.youtube.com/embed/IFIpsXHsGm0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Activities

4 activities are proposed :

[>>>>> the material list is here <<<<<](material-list.md)

| Stretchable kirigami  | Bistable and auxetics mechanical metamaterials  | Auxetics mechanical metamaterials  |
|---|---|---|
|  ![](images/kirigami_web.jpg) | ![](images/bistable_auxetics.jpg)  | ![](images/auxetics_close_web.jpg)  |
| [Tutorial](stretchable.md)  | [Tutorial](bistable.md)  | [Tutorial to adapt](auxetics.md)  |


## Ressources and Development Team

#### Workshop development team:

* Nicolas De Coster (FabLab ULB, Université Libre de Bruxelles ULB, Belgium)
* Axel Cornu (FabLab ULB, Université Libre de Bruxelles ULB, Belgium)
* Pierre-Brice Bintein (ESPCI, Paris, France)
* Victor Levy (FabLab ULB, Université Libre de Bruxelles ULB, Belgium)
* Julien Chopin (Universidade Federal da Bahia, Salvadore, Brazil)
* Denis Terwagne (**coordinator**)


This workshop is a new edition of [a really popular workshop at thh Fab14 conference in Toulouse (France) in 2018.](http://fab.academany.org/2018/labs/fablabulb/FAB14_workshop.html). It is supplemented by a collaboration with Prof. Julien Chopin (UFBA, Brazil) expert in the physics of origami. It was a continuation of a [group project](http://fab.academany.org/2018/labs/fablabulb/#) made during the Fab Academy 2018 [@FablabULB](fablab-ulb.be/) and in relation to scientific research happening [@FrugalLab](frugal.ulb.be/) both at the Université Libre de Bruxelles ULB (Belgium)
Pattern generator scripts were made by Nicolas Decoster during Fab Academy 2018 ([link](http://fab.academany.org/2018/labs/fablabulb/students/nicolas-decoster/?page=assignment&assignment=03))

#### Scientific litterature references:
* [Rafsanjani, Extreme Mechanics Letters, 2016.](https://arxiv.org/pdf/1612.05988.pdf)
* Bertoldi, Nature Reviews Materials, 2017, (original version, postprint).
* B. Florijn, Phys. Rev. Let., 2014 (preprint, original).
* K. Bertoldi, Adv. Mat., 2009, (original version, postprint).
* C. Coulais, Nature Lett., 2016, (preprint, original)
* Isobe, Scientific Reports, 2016.
