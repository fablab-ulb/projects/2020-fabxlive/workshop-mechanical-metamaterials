# Auxetic Mechanical Metamaterials

### Materials

* Acrylic sheets (thicknesses: 5 mm and 8 mm)
* 50+ dowel pins (6, 8 or 10 mm of diameter)
* Scale
* Silicone based polymer (fast curing)
* Mixing cups
* Mixing sticks

### Machines

* Laser cutter
or
* 3D printer

## Designing the mold

In this workshop unit, you will design and fabricate by molding and casting 2D auxetic mechanical metamaterials.

Using [openSCAD](https://www.openscad.org), a free software, the first step is to design the mold. The mold is made of three laser cut acrylic plates (upper, middle, lower) and dowel pins (see photo below).

![](../images/openscad.png)

Here is the openscad code for parametrically designing the upper and lower plates:

```
/*
// FILE    : Auxetics Metamaterials with holes - Upper and lower plates
// VERSION : 0.1 (draft)
// LICENSE : M.I.T. (https://opensource.org/licenses/mit-license.php)
// AUTHOR  : Denis Terwagne
// TARGET  : OpenSCAD
*/

kerf=0;

//dowel pins diameter
diam=8;
//space in between holes
space=diam/5;
//number of holes horizontal and vertical
nb_horiz=7;
nb_vert=7;

base=4;
out_case=10;

//Upper and lower plates

difference(){
square(size=[(nb_horiz+1)*diam+(nb_horiz+1)*space+out_case,nb_vert*diam+(nb_vert-1)*space+2*base+out_case],center=true);
  circle($fn = 200,d=(diam-kerf));

  for (i=[0:1:(nb_vert-1)]){
      translate([0,(diam+space)*(i-(nb_vert-1)/2),0])
      for (i=[0:1:(nb_horiz-1)]){
          translate([(diam+space)*(i-(nb_horiz-1)/2),0,0])circle($fn = 200,d=(diam-kerf));
          translate([-(diam+space)*(i-(nb_horiz-1)/2),0,0])circle($fn = 200,d=(diam-kerf));
      }
  }

}
```

Here is a SVG image example:

![](../images/Auxetic_holes-up-low.svg){width=512px}

Here is the openscad code for the middle plate:

```
/*
  // FILE    : Auxetics Metamaterials with holes - Middle plate
  // VERSION : 0.1 (draft)
  // LICENSE : M.I.T. (https://opensource.org/licenses/mit-license.php)
  // AUTHOR  : Denis Terwagne
  // TARGET  : OpenSCAD
  */

kerf=0;

//dowel pins diameter
diam=8;
//space in between holes
space=diam/5;
//number of holes horizontal and vertical
nb_horiz=7;
nb_vert=7;

base=4;
out_case=10;

//Middle plate
union(){
difference(){
    square(size=[(nb_horiz+1)*diam+(nb_horiz+1)*space+out_case,nb_vert*diam+(nb_vert-1)*space+2*base+out_case],center=true);
    square(size=[(nb_horiz+1)*diam+(nb_horiz+1)*space,nb_vert*diam+(nb_vert-1)*space+2*base],center=true);
    }

   for (i=[0:1:(nb_vert-1)]){
    translate([-(diam+space)*(nb_horiz+1)/2,(diam+space)*(i-(nb_vert-1)/2),0])circle($fn = 200,d=(diam+kerf));
    translate([(diam+space)*(nb_horiz+1)/2,(diam+space)*(i-(nb_vert-1)/2),0])circle($fn = 200,d=(diam+kerf));
   }
}
```
Here is a SVG image example:

![](../images/Auxetic_holes-middle.svg){width=512px}

## Fabricating the mold

To make the mold, laser cut the 3 acrylic plates and prepare a number of dowel pins.


| ![](../images/auxetics-1.jpg)  | ![](../images/auxetics-2.jpg)  |
|---|---|
| Disassembled mold  | Assembled mold. The third plate with holes will be used later.  |



## Molding the Materials

Now, it's time to cast our structure. To do this, follow these steps :

| ![](../images/molding1.jpg)  | ![](../images/molding2.jpg)  | ![](../images/molding3.jpg)  |
|---|---|---|
| Use fast curing silicone rubber.  | Pour an equal amount of BASE and CATALYST using a scale.  | Stir and mix the 2 parts thoroughly. |
| ![](../images/molding4.jpg)  | ![](../images/molding5.jpg)  | ![](../images/molding6.jpg)  |
| Pour the liquid solution in the mold. It might be curing fast so do not loose time... Also if you can vacuum the solution using a vacuum pump and chamber prior pouring that will allow to remove air bubbles. If not, while pouring, make a very thin liquid column by levering up the container with the solution. This will also remove air bubbles.   | Cover the mold with the 3rd acrylic plate. Be careful to avoid air bublles while covering.  | Skeeze and hold the mold in place. |
| ![](../images/molding7.jpg)  | ![](../images/molding8.jpg)  | ![](../images/molding9.jpg)  |
| When it is completely cured. <br> Remove the pins.   | Demold gently the structure.  | Clean your structure by removing excess material. Now, your structure is ready to be tested. |

## Testing the structures

| ![](../images/auxetics_open_web.jpg){width=1024px} | ![](../images/auxetics_close_web.jpg){width=1024px} |
|---|---|
| Hold the sample in between your two hands. | Compress the sample and observe how it shrinks. This is unusual and it is called an auxetic material. Usually when a material is compressed it expands laterally. Auxetic material behave the opposite.  |
