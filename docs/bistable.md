# Bistable Auxetic Kirigami

Materials

* [laserable rubber sheets](https://www.trodat.net/fr-FR/produits/Consommables/Caoutchouc-laser/Pages/produits.aspx) (2 to 3 mm in thickness, no odor if possible)

Machine

* Laser cutter
* cutter or knife

## Introduction

In this part, we are going to make mechanical metamaterials from kirigami. Kirigami is simply a sheet folding in which there are cuts. When the 2D structure is stretched, it deforms in a particular way: It can expand, retract, rotate, move out of plane,...

Here, we will make auxetics materials that are bistable.This means that the structure can take different stable structural geometries.

In the following videos, some researchers have found kirigami patterns that allow this bistability.

<iframe src="https://player.vimeo.com/video/196185494?title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/196185494">Bistable Auxetic Mechanical Metamaterials Inspired by Ancient Geometric Motifs</a> from <a href="https://vimeo.com/user33297838">Ahmad Rafsanjani</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

[Bistable Auxetic Mechanical Metamaterials Inspired by Ancient Geometric Motifs](https://vimeo.com/196185494) from [Ahmad Rafsanjani](https://vimeo.com/user33297838) on Vimeo.

The challenge here, is to find an efficient way to parametrically design all these kind of structures. During Fab Academy 2018, [Nicolas Decoster](http://fab.academany.org/2018/labs/fablabulb/students/nicolas-decoster/) has developped a Vig Flex Wood Pattern Generator during [week 3](http://fab.academany.org/2018/labs/fablabulb/students/nicolas-decoster/?page=assignment&assignment=03). We wanted to push it further and make a kirigami mechanical metamaterial pattern generator.

Carefully analysing these bistable auxetic mechanical metamaterials, we find that there is a beautiful logic behind this puzzle.

They are made of

* a **building block**, the smallest part that is required to build the final structure by rotating, mirroring and translating it.
* a **unit cell** made of several rotated, mirored and translated building blocks.
* the **final structure**, made of multiple translated unit cells (along a squared or triangular latice).

In the figures below, here are 2 parametric designs that we would like to make. For these 2 building blocks, there are 4 parameters: l, the length of the building block, t the hinge gap, a the size of the cut, theta the cut angle.

| ![](../images/bistable_metamaterial-1.png){width=1024px} | ![](../images/bistable_metamaterial-2.png){width=1024px} |
|---|---|
| Square auxetics - squared building block and unit cell in the undeformed and stretched state. [Rafsanjani, Extreme Mechanics Letters, 2016](https://arxiv.org/pdf/1612.05988.pdf). | Triangular auxetics - squared building block and unit cell in the undeformed and stretched state. [Rafsanjani, Extreme Mechanics Letters, 2016](https://arxiv.org/pdf/1612.05988.pdf).  |

## Bistable Auxetic Pattern Generator

<form id="flexForm">
<table>
<tbody>
    <tr>
    <td>Desired width</td>
    <td><input id="width" type="number" min="1" value="100" step="1"></td>
    </tr>
    <tr>
    <td>Desired height</td>
    <td><input id="height" type="number" min="1" value="150" step="1"></td>
    </tr>
    <tr>
    <td>Cell size</td>
    <td><input id="length" type="number" min="1" value="50" step="0.1"></td>
    </tr>
    <tr>
    <td>t</td>
    <td><input id="t" type="number" min="0" value="5" step="0.01"></td>
    </tr>
    <tr>
    <td>&theta;</td>
    <td><input id="theta" type="number" min="0" max ="45" value="15" step="0.1"></td>
    </tr>
    <tr>
    <td>Add border</td>
    <td><input id="isBorder" type="checkbox" checked></td>
    </tr>
    <tr>
    <td>Pattern type</td>
    <td>
        <select id="pattern">
            <option value="1">Square auxetics</option>
            <option value="2" selected>Triangular auxetics</option>
        </select>
    </td>
    </tr>


</tbody>
<tfoot>
  <tr>
  <td colspan="2" style="padding:12px;">
        <p>Note : you must update SVG before copy/download.</p>
        <input type="button" value="See / Update SVG" onclick="updateSVG('auxeticsSVG')">
        <input type="button" value="Copy to clipboard" onclick="copyToClipboard($('#auxeticsSVG').html())">
        <input type="button" value="Download as File" onclick="downloadAsFile('Auxetics_ULB_Generated.svg', $('#auxeticsSVG').html())">
        <input type="button" value="Clear" onclick="$('#auxeticsSVG').html('')">
    </td>
  </tr>
</tfoot>
</table>
</form>
<script src="https://www.vigon.be/NicolasDC.js"></script>
<script>
function updateSVG(svgID){
  var $target   = $("#"+svgID);

  var l      = parseFloat($("#length").val());
  var width   = parseFloat($("#width").val());
  var height   = parseFloat($("#height").val());

  var t     = parseFloat($("#t").val());

  var t_cal = t/l;

  var theta     = parseFloat($("#theta").val())*Math.PI/180;

  var pattern = parseInt(($("#pattern").val()));

  var repX;
  var repY;

  switch(pattern){
    case 1 :
        repX     = Math.ceil(width/l);
        repY     = Math.ceil(height/l);
        break;
    case 2 :
        repX     = Math.ceil(width*2/l);
        repY    = Math.ceil(height/(l*Math.sin(Math.PI/3)));
        break;
  }

  var svgPaths = '<svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="'+width+'mm" height="'+height+'mm" viewBox="0 0 '+width+' '+height+'">'+"\n";
  svgPaths += '<g stroke="black">'+"\n";


  switch(pattern){
    case 1 :
        var TrigoBlock = (t_cal*Math.sin(theta)) + ((t_cal-1)*Math.cos(theta)) + t_cal;
        var x = l*(-Math.cos(theta)*TrigoBlock);
        var y = l*(t_cal-(Math.sin(theta)*TrigoBlock));


        for(var i=0; i < repX; i++){
                for(var j=0; j < repY; j++){
                    svgPaths += "\t"+'<g transform="translate(' + (i*l) + ',' + (j*l) + ')';
                    if((i+j)%2){svgPaths += 'translate('+l+',0)scale(-1,1)'};
                    svgPaths += '">'+"\n";
                    svgPaths += "\t"+'<path d="M '+ '0' +' '+ t + 'L' + x + ' ' + y + '"/>'+"\n";
                    svgPaths += "\t"+'<path d="M '+ (l-t) +' '+ '0' + 'L' + (l-y) + ' ' + x + '"/>'+"\n";
                    svgPaths += "\t"+'<path d="M '+ l +' '+ (l-t) + 'L' + (l-x) + ' ' + (l-y) + '"/>'+"\n";
                    svgPaths += "\t"+'<path d="M '+ t +' '+ l + 'L' + y + ' ' + (l-x) + '"/>'+"\n";
                    svgPaths += "\t"+'</g>'+"\n";

                }
        }
        break;
    case 2 :
        var x = (Math.sin(theta-Math.PI/3)/Math.sin(Math.PI/3))*(t_cal*Math.cos(Math.PI/3 - theta) + (t_cal-1)*Math.cos(theta)) - t_cal*Math.sin(Math.PI/3 - theta);
        var y = Math.tan(theta)*x+t_cal*(Math.sin(Math.PI/3) - Math.tan(theta)*Math.cos(Math.PI/3));
        var startX = t*Math.cos(Math.PI/3);
        var startY = t*Math.sin(Math.PI/3);

        svgPaths += '<g transform="translate(' + (-l/4) + ',0)">';
        for(var i=0; i < repX; i++){
                for(var j=0; j < repY; j++){
                    svgPaths += "\t"+'<g transform="translate(' + (i*l/2) + ',' + (j*l*Math.sin(Math.PI/3)) + ')';
                    if((i+j)%2){svgPaths += 'translate('+l+','+(l/(2*Math.sqrt(3)))+')scale(-1,1)rotate(180, '+(0.5*l)+', '+(l/(2*Math.sqrt(3)))+')'};
                    svgPaths += '">'+"\n";
                    //svgPaths += "\t"+'<path d="M '+ '0' +' '+ '0' + 'L' + l + ' ' + 0 + '"/>'+"\n";                              //see triangle border (debugging purposes)
                    //svgPaths += "\t"+'<path d="M '+ l +' '+ '0' + 'L' + (0.5*l) + ' ' + (l*Math.sin(Math.PI/3)) + '"/>'+"\n";    //see triangle border (debugging purposes)
                    //svgPaths += "\t"+'<path d="M '+ '0' +' '+ '0' + 'L' + (0.5*l) + ' ' + (l*Math.sin(Math.PI/3)) + '"/>'+"\n";  //see triangle border (debugging purposes)
                    svgPaths += "\t"+'<path d="M '+ startX +' '+ startY + 'L' + (x*l) + ' ' + (y*l) + '"/>'+"\n";
                    svgPaths += "\t"+'<g transform="rotate(120, '+(0.5*l)+', '+(l/(2*Math.sqrt(3)))+')"><path d="M '+ startX +' '+ startY + 'L' + (x*l) + ' ' + (y*l) + '"/></g>'+"\n";
                    svgPaths += "\t"+'<g transform="rotate(240, '+(0.5*l)+', '+(l/(2*Math.sqrt(3)))+')"><path d="M '+ startX +' '+ startY + 'L' + (x*l) + ' ' + (y*l) + '"/></g>'+"\n";
                    svgPaths += "\t"+'</g>'+"\n";
                }
        }

        svgPaths += '</g>';
        break;
  }


  if($("#isBorder").prop('checked')){
    svgPaths += '<rect width="'+width+'" height="'+height+'" fill="none" />'+"\n";
  }
  svgPaths += '</g>'+"\n";
  svgPaths += '</svg>'+"\n";
  $target.html(svgPaths);
}
</script>
<div class="text-center">
<!-- Figure that will be filled with the flex Wood script -->
<figure id="auxeticsSVG"></figure>
</div>


## Testing the structures

Using a laser cutter and laserable rubber sheets (2.3 mm in thickness), we managed to make the following bistable auxetic structures:

| ![](../images/square_closed_web.jpg){width=1024px} | ![](../images/square_opened_web.jpg){width=1024px} |
|---|---|
| Unstretched bistable auxetics with square rotating unit. | Stretched bistable auxetics with square rotating unit.  |

| ![](../images/triangular_closed_web.jpg){width=1024px} | ![](../images/triangular_opened_web.jpg){width=1024px} |
|---|---|
| Unstretched bistable auxetics with triangular rotating unit. | Stretched bistable auxetics with triangular rotating unit.  |
